namespace Sources{
	public interface IObjectGenerator<out T>{
		T Create();
	}
}