﻿using System.Collections;
using UnityEngine;

namespace Sources{
	public class Spawner : MonoBehaviour{

		[SerializeField] private ProjectileGenerator _projectileGenerator;
		[SerializeField] private float _interval = 1.0f;
		[SerializeField] private float _speed = 1.0f;

		private ObjectPool<Projectile> _projectilePool;
		
		private void Awake(){
			IObjectGenerator<Projectile> objectGenerator = _projectileGenerator;
			_projectilePool = new ObjectPool<Projectile>(objectGenerator, 10);
			StartCoroutine(Shoot());
		}

		private IEnumerator Shoot(){
			while(true){
				var projectile = _projectilePool.Take();
				projectile.gameObject.SetActive(true);
				projectile.transform.position = transform.position;
				projectile.Initialize(transform.forward, _speed);
				projectile.OnDeath += KillProjectile;
				yield return new WaitForSeconds(_interval);
			}
		}

		private void KillProjectile(Projectile projectile){
			projectile.OnDeath -= KillProjectile;
			projectile.gameObject.SetActive(false);
			_projectilePool.Return(projectile);
		}
	}
}
