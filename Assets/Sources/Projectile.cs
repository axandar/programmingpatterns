﻿using System.Collections;
using UnityEngine;

namespace Sources{
	public class Projectile : MonoBehaviour{

		[SerializeField] private float _life = 3.0f;
		private float _localLife;
		
		public delegate void DeathAction(Projectile projectile);
		public event DeathAction OnDeath;

		private Vector3 _direction;
		private float _speed;

		public void Initialize(Vector3 direction, float speed){
			_direction = direction;
			_speed = speed;
		}

		private void Update(){
			transform.position += _direction * _speed * Time.deltaTime;
			_localLife -= Time.deltaTime;
			if(_localLife < 0){
				if(OnDeath != null){
					StartCoroutine(DestroyObject());
					OnDeath(this);
				}
			}
		}

		private void OnEnable(){
			_localLife = _life;
			StopCoroutine(DestroyObject());
		}

		private IEnumerator DestroyObject(){
			yield return new WaitForSeconds(10);
			Destroy(gameObject);
		}
	}
}
