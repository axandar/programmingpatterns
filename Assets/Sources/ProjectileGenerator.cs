﻿using UnityEngine;

namespace Sources{
	public class ProjectileGenerator: MonoBehaviour, IObjectGenerator<Projectile>{
		[SerializeField] private Projectile _projectile;
		
		public Projectile Create(){
			return Instantiate(_projectile);
		}
	}
}
