using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sources{
	public class ObjectPool<T>{
		private readonly List<T> _unusedObjects;

		private readonly IObjectGenerator<T> _objectGenerator;

		public ObjectPool(IObjectGenerator<T> objectGenerator, int initialCapacity){
			_objectGenerator = objectGenerator;
			_unusedObjects = new List<T>(initialCapacity);
		}

		public T Take(){
			Debug.Log("Number of objects in pool: " + _unusedObjects.Count);
			if(_unusedObjects.Count > 0){
				T obj = _unusedObjects[0];
				Debug.Log("Took object from pool: " + _unusedObjects.Count);
				_unusedObjects.Remove(obj);
				return obj;
			}
			
			CreateNewObject();
			return Take();
		}

		public void Return(T obj){
			_unusedObjects.Add(obj);
			Debug.Log("Returned object: " + _unusedObjects.Count);
			CheckIfRemovingIsNeeded();
		}

		private void CreateNewObject(){
			_unusedObjects.Add(_objectGenerator.Create());
		}

		private void CheckIfRemovingIsNeeded(){
			
		}
	}
}